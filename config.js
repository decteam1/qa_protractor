exports.config = {
	seleniumAddress: 'http://localhost:4444/wd/hub',
	baseUrl: 'https://angularjs.org',

	multiCapabilities: [{
    'browserName': 'internet explorer',
		'nativeEvents': false,},
	{
		'browserName': 'chrome',},
	{
		'browserName': 'firefox',}
],

	specs: ['spec.js',
					'spec-data-provider.js'
	],

	specs: ['spec.js'],

	onPrepare: function() {
		  var jasmineReporters = require('C:/Users/Julio/AppData/Roaming/npm/node_modules/jasmine-reporters');
          jasmine.getEnv().addReporter(new jasmineReporters.JUnitXmlReporter(null, true, true)
          );
     }
};
