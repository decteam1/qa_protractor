'use strict';

var AngularHomePage = require('./PageObjects/AngularHomePage.js');

describe('User accessed AngularJS homepage', function() {

var homePage;

	beforeEach(function() {
	    browser.get(browser.baseUrl);
			homePage = new AngularHomePage();
  	});

	it('homepage should have all its elements displayed', function() {
		expect(homePage.pageTitle.isDisplayed()).toBe(true);
	});

	it('should allow to introduce a name as Julio', function() {
		homePage.setName('Julio');
		expect(homePage.getGreetingMessage()).toEqual('Hello Julio!');
	});

	it('should allow to add new Todo to the list', function(){
		homePage.addNewTodo('TEST');
		var todoList = homePage.getTodoList();


		
		expect(todoList.last().getText()).toEqual('TEST');
	});
});
