# **Protractor exercises and hands-on** #

### What is this repository for? ###

This project is generated in order to gain expertise with Protractor and Jasmine using Javascript in order to implement E2E automated test scenarios.

### How do I get set up? ###

In order to run these tests, these software has been installed:    

* Node.js v6.9.1 - Download for windows  
* Protractor v4.0.11 
```
#!command

npm install -g protractor
```  
  
* Along with Protractor, webdriver manager and Jasmine (v2.5.2) is also installed  

* Jasmine reporters module 
```
#!command

npm install --save-dev jasmine-reporters@^2.0.0
```  
A packaje.json file has been uploaded in order to build the project with the following command in the local folder:
```
#!command

npm install
```
 
### Running tests  

In order to run te tests, just open a new command line and start the webdriver with command: 
```
#!command

webdriver-manager start
```  
In a new command line console go to the directory where the scripts have been downloaded and run command:  

```
#!command

protractor config.js
```  
To check the results, just open the .xml file generated in the same folder where the scripts are located.