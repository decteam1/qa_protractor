'use strict';

var AngularHomePage = require('./PageObjects/AngularHomePage.js');
var data = require('./data.js');
var using = require('jasmine-data-provider');

describe('User accessed AngularJS homepage', function() {

var homePage;

	beforeEach(function() {
	    browser.get(browser.baseUrl);
			homePage = new AngularHomePage();
  	});

	it('homepage should have all its elements displayed', function() {
		expect(homePage.pageTitle.isDisplayed()).toBe(true);
	});

  using(data.names, function(data, description) {

      it("should allow to introduce a name as " + description, function() {
        homePage.setName(data.handle);
        expect(homePage.getGreetingMessage()).toEqual('Hello ' + data.handle + '!');
      })
  });
});
