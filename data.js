'use strict';

module.exports = {
    names: {
        'TEST': {handle: 'TEST'},
        'TEST1': {handle: 'TEST1'},
        'TEST2': {handle: 'TEST2'},
        'TEST3': {handle: 'TEST3'}
    }
}
