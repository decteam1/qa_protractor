'use strict';

var AngularHomePage = function(){
	//Homepage
	this.pageTitle = element(by.xpath("//html/body//img[@class='AngularJS-large']"));
	this.downloadAngular1Button = element(by.xpath("//html/body//a[@class='btn btn-large btn-primary download-btn']"));
	this.downloadAngular2Button = element(by.xpath("//html/body//a[@class='btn btn-large btn-primary ng2-beta']"));

	//The basic section - Todo list
	this.inputName = element(by.model('yourName'));
	this.grettingMessage = element(by.xpath("//html/body//h1[@class = 'ng-binding']"));
	this.inputTodo = element(by.model('todoList.todoText'));
	this.addTodoButton = element(by.xpath("//html/body//form/input[@class='btn-primary']"));
	this.todoList = element.all(by.repeater('todo in todoList.todos'));

	//Top Menu -TODO
// 	html//li[@class='active']
// html/body/header//a[contains(.,'Learn')]
// html/body/header//a[contains(.,'Discuss')]
// html//li[@class='dropdown open']

	this.menuHomeButton =
	this.menuLearnButton =
	this.menudiscussButton =

	this.setName = function(name){
			this.inputName.sendKeys(name);
	};

	this.getGreetingMessage = function(){
			return this.grettingMessage.getText();
	};

	this.addNewTodo = function(newTodo){
		this.inputTodo.sendKeys(newTodo);
		this.addTodoButton.click();
	};

	this.getTodoList = function(){
		return this.todoList;
	}

};

module.exports = AngularHomePage;
